//
//  userTypeDetailsViewController.swift
//  Medico
//
//  Created by Pixster iMac on 10/24/16.
//  Copyright © 2016 Pixster iMac. All rights reserved.
//

import UIKit
import DLRadioButton

class userTypeDetailsViewController: UIViewController, UITableViewDelegate,UITableViewDataSource {
    @IBOutlet weak var tableView: UITableView!

      var usertype : String!
      var registrationDetailsForUser : [String]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        registrationDetailsForUser = ["Village","City","Mobile Number","Speciallity"]
        if usertype != "temporary" || usertype != "Guest Login" {
           
            registrationDetailsForUser.append(contentsOf: ["First Name","Middle Name","Surname","Residence Adress", "Postal Code","Speciality", "Gender","Birth Day"])
        }
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return registrationDetailsForUser.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = self.tableView.dequeueReusableCell(withIdentifier: "cell1", for: indexPath) as! userTypeDetailsTableViewCell
        
        
        
        
        if indexPath.row == 11 {
            cell.detailLabel?.text = registrationDetailsForUser[indexPath.row]+" :"
            cell.detailLabel.sizeToFit()
            
            cell.detailTextField.placeholder = registrationDetailsForUser[indexPath.row]
            cell.detailTextField.placeholderColor = .gray

            
            cell.detailTextField.addTarget(self, action: #selector(userTypeDetailsViewController.forPickingDate(sender:)), for: .editingDidBegin)
        }else if indexPath.row == 10 {
            cell.detailLabel?.text = registrationDetailsForUser[indexPath.row]+" :"
            cell.detailLabel.sizeToFit()
            cell.detailTextField.isHidden = true
            let y = cell.detailTextField.center.y
            let x = cell.detailTextField.frame.origin.x
            let buttongender = DLRadioButton(frame: CGRect(x: x, y: y-5, width: 10, height: 10))
            buttongender.iconColor = .black
            buttongender.indicatorColor = .black
            let buttongender1 = DLRadioButton(frame: CGRect(x: x+70, y: buttongender.frame.origin.y, width: 10, height: 10))
            buttongender1.iconColor = .black
            buttongender1.indicatorColor = .black
            let maleLabel : UILabel = UILabel(frame: CGRect(x: x+20, y: buttongender.frame.origin.y, width: 50, height: 12))
            maleLabel.text = "Male"
            let femaleLabel : UILabel = UILabel(frame: CGRect(x: x+90, y: buttongender.frame.origin.y, width: 70, height: 12))
            femaleLabel.text = "FeMale"
            
            cell.plainView.addSubview(maleLabel)
            cell.plainView.addSubview(femaleLabel)

//            maleLabel.adjustsFontSizeToFitWidth = true
//            femaleLabel.adjustsFontSizeToFitWidth = true
            cell.plainView.addSubview(buttongender)
            cell.plainView.addSubview(buttongender1)

            
            
        }
        else{
            
            cell.detailLabel?.text = registrationDetailsForUser[indexPath.row]+" :"
            cell.detailLabel.sizeToFit()
            
            cell.detailTextField.placeholder = registrationDetailsForUser[indexPath.row]
            cell.detailTextField.placeholderColor = .gray
            
        }

        return cell
    }
    func datePickerValueChanged(sender:UIDatePicker) {
        
        let cell = tableView.cellForRow(at: IndexPath(row: 11, section: 0)) as! userTypeDetailsTableViewCell
        
        let dateFormatter = DateFormatter()
        
        dateFormatter.dateStyle = DateFormatter.Style.medium
        
        dateFormatter.timeStyle = DateFormatter.Style.none
        
        cell.detailTextField.text = dateFormatter.string(from: sender.date)
        
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print(indexPath)
    }
    func forPickingDate(sender: UITextField) {
    
        
        let datePickerView:UIDatePicker = UIDatePicker()
        
        datePickerView.datePickerMode = UIDatePickerMode.date
//        tableView.frame.size.height = self.view.frame.size.height - datePickerView.frame.size.height
        sender.inputView = datePickerView
        
        let toolBar = UIToolbar()
        toolBar.barStyle = UIBarStyle.default
        toolBar.isTranslucent = true
        toolBar.tintColor = UIColor(red: 76/255, green: 217/255, blue: 100/255, alpha: 1)
        toolBar.sizeToFit()
        
        let doneButton = UIBarButtonItem(title: "Done", style: UIBarButtonItemStyle.plain, target: self, action: #selector(userTypeDetailsViewController.donePressed(sender:)))
        
        
        
        let spaceButton = UIBarButtonItem(barButtonSystemItem: UIBarButtonSystemItem.flexibleSpace, target: nil, action: nil)
        let cancelButton = UIBarButtonItem(title: "Cancel", style: UIBarButtonItemStyle.plain, target: self, action: #selector(userTypeDetailsViewController.donePressed))
        
        toolBar.setItems([cancelButton, spaceButton, doneButton], animated: false)
        toolBar.isUserInteractionEnabled = true
        sender.inputAccessoryView = toolBar

        
        datePickerView.addTarget(self, action: #selector(userTypeDetailsViewController.datePickerValueChanged), for: UIControlEvents.valueChanged)
    }
    func donePressed(sender : UIDatePicker){
        
        
        
        
        print("Aiyub")
        
        
        view.endEditing(true)
        
    }
    
    func forNotPickingDateOther(sender: UITextField) {
        
//        tableView.frame.size.height = self.view.frame.size.height
//        tableView.reloadData()
       
    }
   
    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
