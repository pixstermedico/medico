//
//  RegisterDetailsTableViewCell.swift
//  Medico
//
//  Created by Pixster iMac on 10/24/16.
//  Copyright © 2016 Pixster iMac. All rights reserved.
//

import UIKit
import DropDown

class RegisterDetailsTableViewCell: UITableViewCell {
    @IBOutlet weak var detailLabel: UILabel!

    @IBOutlet weak var detailTextField: MinoruTextField!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
     
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
