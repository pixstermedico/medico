//
//  RegisterDetailsViewController.swift
//  Medico
//
//  Created by Pixster iMac on 10/24/16.
//  Copyright © 2016 Pixster iMac. All rights reserved.
//

import UIKit
import DropDown

class RegisterDetailsViewController: UIViewController,UITableViewDataSource,UITableViewDelegate {
    
//    @IBOutlet weak var createAccountButton: UIButton!
    var registrationDetails : [String]!
//    var dropDown = DropDown()

    @IBOutlet weak var tableView: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
        registrationDetails = ["E-mail","Password","Confirm Password","Registration Type","User Type"]
        
//        dropDown.anchorView = createAccountButton
//        dropDown.bottomOffset = CGPoint(x: 0, y:(dropDown.anchorView?.plainView.bounds.height)!)
//        dropDown.dataSource = ["Guest", "Temporary","Permenant"]
        
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return registrationDetails.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "cell", for: indexPath) as! RegisterDetailsTableViewCell
//        dropDown.direction = .bottom
        cell.detailLabel?.text = registrationDetails[indexPath.row]+" :"
        
        cell.detailLabel.sizeToFit()
        cell.detailTextField.placeholder = registrationDetails[indexPath.row]
        cell.detailTextField.placeholderColor = .gray
        
//        DropDown.appearance().textFont = UIFont.systemFont(ofSize: 14)
//        self.dropDown = DropDown()
//        self.dropDown.anchorView = cell.detailTextField
        // this is the textfield
//        self.dropDown.dataSource = ["Guest", "Temporary","Permenant"]
//        dropDown.selectionAction = {(index: Int, item: String) -> Void in
//            strongify(self)
//            self.currentDomain = item
//            (cell.detailTextField.rightView! as! UIButton).setTitle(item, for: .normal)
//        }

        
        
        
        return cell
    }
    

    @IBAction func nextPressed(_ sender: AnyObject) {
        
         self.performSegue(withIdentifier: "showUserTypeDetail", sender: self)
    }
    
    // MARK: - Navigation
//     In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.identifier == "showUserTypeDetail" {
            
            let vc = segue.destination as! userTypeDetailsViewController
            
            let cell = tableView.cellForRow(at: IndexPath(row: 4, section: 0)) as! RegisterDetailsTableViewCell
            
            vc.usertype = cell.detailTextField.text
            
            
            
        }
    }
    
}

        
//         Get the new view controller using segue.destinationViewController.
//         Pass the selected object to the new view controller.

