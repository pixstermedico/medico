//
//  ViewController.swift
//  Medico
//
//  Created by Pixster iMac on 10/24/16.
//  Copyright © 2016 Pixster iMac. All rights reserved.
//

import UIKit

class ViewController: UIViewController {

//    @IBOutlet weak var EmailTextField: !
    @IBOutlet weak var submitButton: UIButton!
    @IBOutlet weak var emailTextField: MinoruTextField!
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var otpTextField: MinoruTextField!
    @IBOutlet weak var verifyButton: UIButton!
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.tintColor = UIColor(colorLiteralRed: 82/255, green: 122/255, blue: 176/255, alpha: 1.0)
        submitButton.isHidden = true
        otpTextField.isHidden = true
        emailTextField.placeholderColor = .gray
        otpTextField.placeholderColor = .gray
        emailTextField.placeholder = "Enter Your E-mail"
        otpTextField.placeholder = "Enter Confermation Code"
        
        
        
        
        
        
        let tapper = UITapGestureRecognizer(target: view, action:#selector(UIView.endEditing))
        tapper.cancelsTouchesInView = false
        view.addGestureRecognizer(tapper)
        // Do any additional setup after loading the view, typically from a nib.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    func isValidEmail(testStr:String) -> Bool {
        // print("validate calendar: \(testStr)")
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format:"SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    
    
    
    @IBAction func verifyPressed(_ sender: AnyObject) {
        
        
        //if email is valid
        
       
        emailTextField.isHidden = true
        emailLabel.text = "Enter Confirmation Code:"
        emailLabel.sizeToFit()
        verifyButton.isHidden = true
        submitButton.isHidden = false
        otpTextField.isHidden = false
        
        
        
        
        
        
    }
    @IBAction func submitButtonPressed(_ sender: AnyObject) {
        
        self.performSegue(withIdentifier: "ShowRegisterDetails", sender: self)
    }
    
}

